Facter.add(:heartbeat_addr) do
  setcode do
    hbhostname = "hb" + Facter.value(:hostname)
    begin
      ib_addr = Resolv.getaddress hbhostname
      ip = ib_addr
    rescue => e
      ip = '169.254.0.0'
    end
    ip
  end
end
