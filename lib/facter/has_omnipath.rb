Facter.add(:has_omnipath) do
	confine :kernel => "Linux"
	setcode do
		Facter::Core::Execution.execute('lspci | grep -i omni-path').to_s != ''
	end
end
