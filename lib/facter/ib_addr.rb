Facter.add(:ib_addr) do
  setcode do
    ibhostname = "ib" + Facter.value(:hostname)
    begin
      ib_addr = Resolv.getaddress ibhostname
      ip = ib_addr
    rescue => e
      ip = '169.254.0.0'
    end
    ip
  end
end
