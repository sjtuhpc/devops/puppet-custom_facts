Facter.add(:ipmi_addr) do
  setcode do
    ipmihostname = "ipmi-" + Facter.value(:hostname)
    begin
      ipmi_addr = Resolv.getaddress ipmihostname
      ip = ipmi_addr
    rescue => e
      ip = '169.254.0.0'
    end
    ip
  end
end
