Facter.add(:eth_if) do
  setcode do
    eth_if = Facter.value(:networking)['interfaces'].keys.sort[0]
    if eth_if.include? 'br'
      ''
    else
      eth_if
    end
  end
end
