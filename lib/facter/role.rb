Facter.add(:puppet_role) do
  setcode do
    case Facter.value(:hostname)
    when /^(mail|proxy|mongodb|acct|vpn|l2tp|puppetserver|paramon|robinhood|prometheus|mysql|pgsql|lb|slurmdbd|grafana|yp|license)$/
      Facter.value(:hostname)
    when /^(ns[1-2])$/
      'ns'
    when /^(slurm[1-2])$/
      'slurmctld'
    when /^(vm[0-4])$/
      'hypervisor'
    when /^(nfs[1-2])$/
      'nfs'
    when /^(dellmds[1-2]|delloss0[1-6])$/
      'delllustre'
    when /^(gluster[1-2])$/
      'gluster'
    when /^centos.*$/
      'template'
    when /^(arvados)$/
      'arvados'
    when /^(help)$/
      'help'
    when /^(piopenvpn)$/
      'openvpn'
    else
      'compute'
    end
  end
end
