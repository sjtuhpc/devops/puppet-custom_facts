
Facter.add(:puppet_subtype) do
  setcode do
    case Facter.value(:hostname)
    when /^(mu0[4-7])$/
      'login'
    when /^(nv0[39])$/
      'p100'
    when /^node(30[0-8]|[12][0-9]{2}|0?[1-9][0-9]|0?0?[1-9])$/ # node[001-308]
      'cpu'
    when /^nv(04|12)$/ # nv04 and nv12 are cpu nodes due to failures of GPU cards on them
      'cpu'
    when /^node(33[0-2]|3[12][0-9]|309)$/ # node[309-332]
      'cpu128'
    when /^gpu[0-5][0-9]$/
      'gpu'
    when /^fat[0-2][0-9]$/
      'fat'
    when /^mic0[1-5]$/
      'k40'
    when /^nv[0-1][0-9]$/
      'k80'
    when /^knl0[0-4]$/
      'knl'
    when /^huge[1-9]$/
      'huge'
    else
      'unknown'
    end
  end
end
