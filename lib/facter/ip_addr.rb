require 'resolv'

Facter.add(:ip_addr) do
  setcode do
    hostname = Facter.value(:hostname)
    ip = Resolv.getaddress hostname
    ip
  end
end
