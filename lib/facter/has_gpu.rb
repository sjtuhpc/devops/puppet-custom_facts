Facter.add(:has_gpu) do
	confine :kernel => "Linux"
	setcode do
		Facter::Core::Execution.execute('lspci -d 10de: | grep -i nvidia').to_s != ''
	end
end
