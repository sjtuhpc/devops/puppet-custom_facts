Facter.add(:has_infiniband) do
	confine :kernel => "Linux"
	setcode do
		Facter::Core::Execution.execute('lspci | grep -i connectx').to_s != ''
	end
end
